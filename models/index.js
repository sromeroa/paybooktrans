const Sequelize = require('sequelize');

const sequelize = new Sequelize('oorden', 'root', '', {
  host: '127.0.0.1',
  dialect: 'mysql',

  pool: {
    max: 5,
    min: 0,
    idle: 10000
  },
});


var db=[];

db.Transaction = sequelize.define('PaybookTransaction', {
		id_transaction: {
		type: Sequelize.STRING(36)
		},
		id_account: {
		type: Sequelize.STRING(36)
		},
		id_account_type: {
		type: Sequelize.STRING(36)
		},
		id_currency: {
		type: Sequelize.STRING(36)
		},
		id_external: {
		type: Sequelize.STRING(100)
		},
		id_site: {
		type: Sequelize.STRING(36)
		},
		id_site_organization: {
		type: Sequelize.STRING(36)
		},
		id_site_organization_type: {
		type: Sequelize.STRING(36)
		},
		id_user: {
		type: Sequelize.STRING(36)
		},
		is_disable: {
		type: Sequelize.STRING(3)
		},
		description: {
		type: Sequelize.STRING(500)
		},
		amount: {
		type: Sequelize.DECIMAL(18,6)
		},
		currency: {
		type: Sequelize.STRING
		},
		attachments: {
		type: Sequelize.STRING
		},
		extra: {
		type: Sequelize.STRING(500)
		},
		reference: {
		type: Sequelize.STRING
		},
		keywords: {
		type: Sequelize.STRING
		},
		dt_transaction: {
		type: Sequelize.DATE
		},
		dt_refresh: {
		type: Sequelize.DATE
		},
		dt_disable: {
		type: Sequelize.DATE
		},
		ApiKEY: {
			type: Sequelize.STRING(36)
		},
		token: {
			type: Sequelize.STRING(36)
		}
});








db.Users = sequelize.define('PaybookUser', {
		id_user: {
			type:Sequelize.STRING(36), primaryKey: true
		},
		id_external: {
			type: Sequelize.STRING(36)
		},
		ApiKEY: {
			type: Sequelize.STRING(36)
		},
		name: {
			type: Sequelize.STRING
		},
		dt_create: {
			type: Sequelize.DATE
		},
		dt_modify: {
			type: Sequelize.DATE
		},
		active:{
			type: Sequelize.STRING(3)
		}
})



db.Session = sequelize.define('PayBookSession', {
		id_user: {
			type:Sequelize.STRING(36)
		},
		token: {
			type: Sequelize.STRING(36)
		},
		key: {
			type: Sequelize.STRING(100)
		},
		iv: {
			type: Sequelize.STRING(36)
		},		
		ApiKEY: {
			type: Sequelize.STRING(36)
		},

})


db.Sites = sequelize.define('PaybookSite', {
		id_site: {
			type:Sequelize.STRING(36)
		},
		id_site_organization: {
			type: Sequelize.STRING(36)
		},
		id_site_organization_type: {
			type: Sequelize.STRING(36)
		},
		is_business: {
			type: Sequelize.STRING(3)
		},
		is_personal: {
			type: Sequelize.STRING(3)
		},		
		name: {
			type: Sequelize.STRING
		},
		ApiKEY: {
			type: Sequelize.STRING(36)
		},
})


db.Credential= sequelize.define('PaybookCredential', {
		token: {
			type: Sequelize.STRING(36)
		},
		id_site: {
			type:Sequelize.STRING(36)
		},
		id_credential: {
			type:Sequelize.STRING(36)
		},
		id_job_uuid: {
			type: Sequelize.STRING(36)
		},
		id_job: {
			type: Sequelize.STRING(36)
		},
		username: {
			type: Sequelize.STRING(35)
		},
		ws: {
			type: Sequelize.STRING(80)
		},		
		status: {
			type: Sequelize.STRING(80)
		},
		twofa: {
			type: Sequelize.STRING(80)
		},
		is_authorized: {
			type: Sequelize.STRING(3)
		},
		is_locked: {
			type: Sequelize.STRING(3)
		},
		is_twofa: {
			type: Sequelize.STRING(3)
		},
		can_sync: {
			type: Sequelize.STRING(3)
		},
		username: {
			type: Sequelize.STRING(80)
		},	
		keywords: {
			type: Sequelize.STRING
		},	
		dt_refresh: {
			type: Sequelize.DATE
		},	
		dt_execute: {
			type: Sequelize.DATE
		},	
		dt_last_sync: {
			type: Sequelize.DATE
		},																		
})



db.Acount= sequelize.define('PaybookAcount', {
		id_account: {
			type: Sequelize.STRING(36)
		},
		id_account_type: {
			type:Sequelize.STRING(36)
		},
		id_currency: {
			type:Sequelize.STRING(36)
		},
		id_user: {
			type: Sequelize.STRING(36)
		},
		id_external: {
			type: Sequelize.STRING(36)
		},
		id_credential: {
			type: Sequelize.STRING(36)
		},
		id_site: {
			type: Sequelize.STRING(36)
		},		
		id_site_organization: {
			type: Sequelize.STRING(36)
		},
		id_site_organization_type: {
			type: Sequelize.STRING(36)
		},
		is_disable: {
			type: Sequelize.STRING(5)
		},
		name: {
			type: Sequelize.STRING(250)
		},
		number: {
			type: Sequelize.STRING(80)
		},
		balance: {
			type: Sequelize.DECIMAL(18,6)
		},
		currency: {
			type: Sequelize.STRING(20)
		},
		account_type: {
			type: Sequelize.STRING(80)
		},
		site: {
			type: Sequelize.STRING(36)
		},
		extra: {
			type: Sequelize.STRING(100)
		},
		keywords: {
			type: Sequelize.STRING(100)
		},
		dt_refresh: {
			type: Sequelize.DATE
		},
		ApiKEY: {
			type: Sequelize.STRING(36)
		},
		token: {
			type: Sequelize.STRING(36)
		}
		

})

module.exports=db;


