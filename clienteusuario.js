var db=require('./models')
var pb = require("paybook");
pb.api.setApiKey("f6a653117a47846a54aa8dc5348c15b0");
//pb.api.setApiKey("0ba3f4260178b9f9a7955fd9185086f6");//// PRODUCCION

/*var pb = require("paybook");
//pb.api.setApiKey("0ba3f4260178b9f9a7955fd9185086f6");//// PRODUCCION
pb.api.setApiKey("ef333901084894c3318a0ad755e93b72");//// PRUEBAS 

*/

pb.api.setTest(true); 

sApiKey="f6a653117a47846a54aa8dc5348c15b0";

var option=process.argv[2];
switch(option) {
	case "createUser":
		var id_user=createUser(process.argv[3])
		console.log("createUser");
	break;


	case "getUsers":
		var searchUS=process.argv[3]
		GetUsers(searchUS);
		console.log("getUsers");
	break;


	case "dropUser":
		var searchUS=process.argv[3]
		dropUser(searchUS);
		console.log("dropUser");
	break;	


	case "createSession":
	var id_user=process.argv[3]
	createSession(id_user);
	break;


	case "getSites":
	var token=process.argv[3];
	getSites(token);
	break;


	case "createCredentials":
	var token=process.argv[3];
	var site=process.argv[4];
	var username=process.argv[5];
	var password=process.argv[6];
	createCredentials(token,site,username,password);
	break;


	case "getTransaction":
	var token=process.argv[3];
	getTransactions(token);
	break;


	case "getAccounts":
	var token=process.argv[3];
	getAccounts(token);
	break;


	case "sendTwofa":
	var token=process.argv[3];
	var site=process.argv[4];
	sendTwofa(token,site);
	break;


	case "getCredentials":
	var token=process.argv[3];
	getCredentials(token);
	break;


	default:
	console.log("PARAMETRO NO VALIDO");
}


function createUser(userName){
	pb.api.createUser(userName,null,function(err,response){
		if(err){
			console.log(err,response); 
			return;
		}
		dt_create_d = new Date(1000*response.dt_create);
		dt_modify_d = new Date(1000*response.dt_modify);
		db.Users.sync({force: false});
		const saveUser = db.Users.build({
		id_user: response.id_user,
		id_external: response.id_external,
		ApiKEY:sApiKey,
		name: response.name,
		dt_create: dt_create_d,
		dt_modify: dt_modify_d,
		active: "1"})
		saveUser.save().catch(error => {
		console.log(error.errors);
		return error.errors;
		})
		console.log(">>>>>"+response.id_user);
	});
}

function createSession(idUser){
	session=pb.api.createSession(idUser, function(err,resp){
		if(err){
			console.log(err); 
			return;
		}
		console.log(resp);
		db.Session.sync({force: false});
		const saveSession = db.Session.build({
			id_user: idUser,
			token: resp.token,
			key: resp.key,
			iv: resp.iv,
			ApiKEY:sApiKey,
		});
		saveSession.save().catch(error => {
			console.log(error.errors);
		})
	})
}

/////  crear credencales 
function createCredentials(tokenI,siteI, username, password){
	credentials= {"username":username,"password":password};
	pb.api.credentials(tokenI,siteI,credentials, function(err,resp){
			if(err){
				console.log(err); 
				return;
			}
			console.log(resp);
			db.Credential.sync({force: false});
			const saveSession = db.Credential.build({
			token:tokenI,
			id_site:siteI,
			id_credential: resp.id_credential,
			id_job_uuid: resp.id_job_uuid,
			id_job: resp.id_job,
			ws: resp.ws,
			username: resp.username,
			status: resp.status,
			twofa: resp.twofa,
			ApiKEY:sApiKey,
			});
			saveSession.save().catch(error => {
				console.log(error.errors);
			})
			getCredentials(tokenI);
	})
}

function getCredentials(token){
	pb.api.getCredentials(token, function(err, credentials){
		if(err){console.log(err);
			return;
			}
		credentials.response.forEach(function(credential){
			var dt_refresh_d=null
			var dt_execute_d=null
			var dt_last_sync_d=null
			if(credential.dt_refresh)dt_refresh_d = new Date(1000*credential.dt_refresh);
			if(credential.dt_execute)dt_execute_d = new Date(1000*credential.dt_execute);
			if(credential.dt_last_sync)dt_last_sync_d = new Date(1000*credential.dt_last_sync);
			db.Credential.update({
				is_authorized:credential.is_authorized ,
				is_locked:credential.is_locked ,
				is_twofa:credential.is_twofa ,
				can_sync:credential. can_sync,
				username:credential.username ,
				keywords:credential.keywords ,
				dt_refresh:dt_refresh_d,
				dt_execute:dt_execute_d,
				dt_last_sync:dt_last_sync_d,
			},{ where: {id_credential: credential.id_credential} }).then(() => {})
		})
	})
}

// VER SITES 
function getSites(token){
	pb.api.cataloguesSites(token,function(err,resps){
		if(err){
			console.log(err); 
			return;
		}
		db.Sites.sync({force: false});
		sites=resps.response;
		sites.forEach(function(site){
			db.Sites.findOne({ where: {id_site: site.id_site} }).then(ste => {
				if (!ste) {
					const saveSite = db.Sites.build({
						id_site: site.id_site,
						id_site_organization: site.id_site_organization,
						id_site_organization_type: site.id_site_organization_type,
						is_business: site.is_business,
						is_personal: site.is_personal,
						name: site.name,
						ApiKEY:sApiKey,
					});
					saveSite.save().catch(error => {
						console.log(error.errors);
					})
				}
			})
		})
	})  
}




function GetUsers(searchUS){
	pb.api.getUsers(null,function(err,resp){
		if(err){
			console.log(err); 
			return;
		}
		db.Users.sync({force: false});
		users=resp.response;
		users.forEach(function(user){
			dt_create_d = new Date(1000*user.dt_create);
			dt_modify_d=null
			if(user.dt_modify)dt_modify_d = new Date(1000*user.dt_modify);

			db.Users.findOne({ where: {id_user: user.id_user} }).then(us => {
				if (!us) {
					dt_create_d = new Date(1000*user.dt_create);
					dt_modify_d = new Date(1000*user.dt_modify);
					const saveUsuario = db.Users.build({
						id_user: user.id_user,
						id_external: user.id_external,
						name: user.name,
						dt_create: dt_create_d,
						dt_modify: dt_modify_d,
						active: "1"
					});
					saveUsuario.save().catch(error => {
						console.log(error.errors);
					})
				}

			})
			if (searchUS){
				if (user.name==searchUS) {console.log(user.name)}
			};
		})
	})
}


function dropUser(searchUS){
	db.Users.findOne({ where: {id_user: searchUS} }).then(us => {
		console.log(">>"+us.id_user)
		db.Users.update({active: '0'},{ where: {id_user: us.id_user} }).then(() => {})
		pb.api.deleteUser(us.id_user,function(err,resp){
		if(err){
			console.log(err); 
			return;
		}
		})
	})
}

function sendTwofa(token, site){
	//token="f3f6975e7b8aff6af4738cb9132326bb";
	//site="56cf5728784806f72b8b4569";
	//var twofa=[];
	twofa={"token":"test"}
	//token="c37d193d7d267c876c99c85068e0a8d2";
	var url="";

	db.Credential.findOne({ where: {token: token} }).then(tf => {
		url=tf.twofa;
		console.log(url);
		pb.api.submitTwofa(url, token, site, twofa, function(err,resp){
			if(err){
				console.log(err); 
				return;
			}
			console.log(resp);
		})
	})
}

function getTransactions(tokenI){
	pb.api.getTransactions(tokenI,null, function(err,transacciones){
			if(err){
				return;
			}
	console.log(transacciones)
		db.Transaction.sync({force: false});
		transacciones=transacciones.response;
		transacciones.forEach(function(trans){

			dt_transaction_d = new Date(1000*trans.dt_transaction);
			dt_refresh_d = new Date(1000*trans.dt_refresh);
			dt_disable_d=null;
			if(trans.dt_disable)dt_disable_d = new Date(1000*trans.dt_disable);
			
			db.Transaction.findOne({ where: {id_transaction: trans.id_transaction} }).then(tr => {
				if (!tr) {
					const saveTransaction = db.Transaction.build({
						id_transaction: trans.id_transaction,
						id_account: trans.id_account,
						id_account_type: trans.id_account_type,
						id_currency: trans.id_currency,
						id_external: trans.id_external,
						id_site: trans.id_site,
						id_site_organization: trans.id_site_organization,
						id_site_organization_type: trans.id_site_organization_type,
						id_user: trans.id_user,
						is_disable: trans.is_disable,
						description: trans.description,
						amount: parseInt(trans.amount),
						currency: trans.currency,
						//attachments: "trans.attachments,",
						extra: trans.extra,
						reference: trans.reference,
						keywords: trans.keywords,
						dt_transaction: dt_transaction_d,
						dt_refresh: dt_refresh_d,
						dt_disable: dt_disable_d,
						ApiKEY:sApiKey,
					 	token,tokenI
					});
					saveTransaction.save().catch(error => {
						console.log(error.errors);
					})
				}
			})
		})

	})	
}



function getAccounts(tokenI){
	pb.api.getAccounts(tokenI,null,function(err,cuentas){
		if(err){
			console.log(err);
			return;
		}
		console.log(cuentas);
		cuentas=cuentas.response;
		db.Acount.sync({force:false})
		cuentas.forEach(function(cuenta){
			dt_refresh_d=null;
			if(cuenta.dt_refresh)dt_refresh_d = new Date(1000*cuenta.dt_refresh);
			db.Acount.findOne({ where: {id_account: cuenta.id_account} }).then(ac => {
					if (!ac) {
						const saveAcount = db.Acount.build({
							id_account:cuenta.id_account,
							id_account_type: cuenta.id_account_type,
							id_currency: cuenta.id_currency,
							id_user: cuenta.id_user,
							id_external: cuenta.id_external,
							id_credential: cuenta.id_credential,
							id_site:cuenta.id_site,
							id_site_organization:cuenta.id_site_organization,
							id_site_organization_type:cuenta.id_site_organization_type ,
							is_disable:cuenta.is_disable ,
							name: cuenta.name,
							number:cuenta.number, 
							balance: cuenta.balance,
							currency:cuenta.currency, 
							account_type:cuenta.account_type ,
							site:cuenta.site.id_site ,
							extra:cuenta.extra ,
							keywords:cuenta.keywords,
							dt_refresh:dt_refresh_d,
							ApiKEY:sApiKey,
							token:tokenI,
						});
						saveAcount.save().catch(error=>{
							console.log(error.errors)
						})
				}
				else{
					db.Acount.update({
					id_account:cuenta.id_account,
					id_account_type: cuenta.id_account_type,
					id_currency: cuenta.id_currency,
					id_user: cuenta.id_user,
					id_external: cuenta.id_external,
					id_credential: cuenta.id_credential,
					id_site:cuenta.id_site,
					id_site_organization:cuenta.id_site_organization,
					id_site_organization_type:cuenta.id_site_organization_type ,
					is_disable:cuenta.is_disable ,
					name: cuenta.name,
					number:cuenta.number, 
					balance: cuenta.balance,
					currency:cuenta.currency, 
					account_type:cuenta.account_type ,
					site:cuenta.site.id_site ,
					extra:cuenta.extra ,
					keywords:cuenta.keywords,
					dt_refresh:dt_refresh_d,
					ApiKEY:sApiKey,
					token:tokenI,},{ where: {id_account: ac.id_account} }).then(() => {})
				}
			})
		})
	})
}	



